#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

device="#($CURRENT_DIR/scripts/bond_interface.sh)"
interpolate_string="\#{bond_device}"

interpolate() {
	local -r status="$1"
	local -r status_value=$(tmux show-option -gqv "$status")
	tmux set-option -gq "$status" "${status_value/$interpolate_string/$device}"
}

main() {
	interpolate "status-left"
	interpolate "status-right"
}

main
