# tmux bond device

If you have a network bond setup between ethernet and your Wi-Fi device and you like to know which device is currently active from the comfort of your Tmux session then this plugin is for you.

## Usage
Add the format string `#{bond_device}` in to your `status-right` or `status-left` option.

Example:
```
set-option -g status-right 'Current bond device: #{bond_device} | %Y-%m-%d %H:%M ' 
```

By default the output will be `ethernet` if the current device is Ethernet, or `wi-fi` if the current device is Wi-Fi.
You can modify the output using the options `@ethernet` and `@wifi`.
```
set -g @ethernet "hardwire"
set -g @wifi "802.11"
```

You may also wish to change the default bond device from `bond0` by using the `@bond` option
```
set -g @bond "bond1"
```

## Installation
### With [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm) (recommended)
Add the plugin to your list of TPM plugins in `.tmux.conf`
```
set -g @plugin 'https://gitlab.com/pyratebeard/tmux-bond-device.git'
```

Fetch the plugin and source it using `prefix + I`.

### Manual installation
Clone the repo
```
git clone https://gitlab.com/pyratebeard/tmux-bond-device.git ~/clone/path/
```

Add this line to the bottom of your `.tmux.conf`
```
run-shell ~/clone/path/bond_device.tmux
```

Reload your Tmux environment by typing the following in the terminal
```
tmux source-file ~/.tmux.conf
```

## Interval
The speed at which the bond_device option changes is based on your `status-interval` setting in Tmux.

To set a lower number enter the following in your `.tmux.conf`
```
set -g status-interval 5
```

## Lengthen status
If you find the additional information in your status bar, especially `status-right`, is being cut off you may need to lengthen the status bar field

Enter the following in your `.tmux.conf`, changing the value as required
```
set -g status-right-length 100
```

## License
[MIT](LICENSE)
