#!/usr/bin/env bash

bond_device="bond0"
ethernet="ethernet"
wifi="wi-fi"
bond_option="@bond"
ethernet_option="@ethernet"
wifi_option="@wifi"

get_tmux_option() {
	local -r option="$1"
	local -r default_option="$2"
	local -r option_value="$(tmux show-option -gqv "$option")"
	if [ -z "$option_value" ] ; then
		echo "$default_option"
	else
		echo "$option_value"
	fi
}

network_device() {
	local -r bond_device="$(get_tmux_option "$bond_option" "$bond_device")"
	local current_device="$(awk '/Currently Active/ {print $NF}' /proc/net/bonding/${bond_device})"
	if [[ $current_device =~ ^e ]] ; then
		current_device="$(get_tmux_option "$ethernet_option" "$ethernet")"
	elif [[ $current_device =~ ^w ]] ; then
		current_device="$(get_tmux_option "$wifi_option" "$wifi")"
	fi
	
	echo "$current_device"
}

main() {
	network_device
}

main
